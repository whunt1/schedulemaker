class ScheduleRequest():
    """Class to contain request data"""

    def __init__(self, netID, num_desired_shifts, available_shifts
                    service_preference, day_preference, time_preference
                    tier, name):
        
        self.netID                  = netID 
        self.num_desired_shifts     = desired_shifts
        self.num_assigned_shifts    = 0
        self.available_shifts       = available_shifts
        self.assigned_shifts        = [[],[],[],[],[],[]]
        self.service_preferences    = service_preference
        self.day_preferences        = day_preferences
        self.time_preferences       = time_preferences
        self.employee_tier          = tier
        self.employee_name          = name

    @property
    def percent_desired(self):
        """Calculates the employee's happiness with quantity of assigned shifts"""
        return self.num_assigned_shifts / self.num_desired_shifts

    @property
    def assigned_quality(self):
        """Calculates the employee's perceived happiness with quality of assigned shifts"""
        min_priority = 5
        max_priority = 0
        total_assigned = 0
        for day, num in enumerate(self.assigned_shifts):
            for shift in day:
                priority = self.get_priority(num, shift)
                total_assigned += priority
                if priority > max_priority:
                    max_priority = priority

        avg_priority = total_assigned / self.num_assigned_shifts
        mid_scale = max_priority / 2
        return avg_priority / mid_scale

    @property
    def TOD_satisfaction(self):
        pass

    @property
    def DOW_satisfaction(self):
        pass

    @property
    def service_satisfaction(self):
        pass

    @property
    def satisfaction(self):
        pass

    def __str__(self):
        return self.netID + "(" + self.name + ")"

def read_file(file_name):


