from . import slot

class Shift():

    def __init__(self, start_time, service):
        self.start_time = start_time
        self.service = service
        self.slots = make_slots(start_time, service)

    @property
    def time_section(self):
        if self.start_time < 16:
            return "morning"
        else if self.start_time > 28:
            return "night"
        else return "day"

    @property
    def day(self):
        return 
def make_slots(start_time, helpline=False):
    if helpline:
        return [Slot(), Slot()]
    else if start_time > 8 and start_time < 28:
        return [Slot(), Slot(), Slot()]
    else:
        return [Slot(), Slot()]
    
