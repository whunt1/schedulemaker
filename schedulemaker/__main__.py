from openpyxl import load_workbook, Workbook
from openpyxl.utils import get_column_letter
import os
import argparse

def main():

    #Set up argument parser
    parser = argparse.ArgumentParser(
            description="A tool to automagically produce optimal schedules",
            prog="Sched")
    parser.add_argument("-r", "--requests", help="Directory to search for requests",
            default=os.getcwd())
    parser.add_argument("-e", "--employees", help="File to use as employee data")
    args = parser.parse_args()

    #

if __name__ == "__main__":
    main()
