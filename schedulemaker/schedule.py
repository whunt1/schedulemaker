class Schedule():

    def __init__():
        self.week = make_week()

def make_week():
    weekday_shifts  = 13 * 4
    sunday_shifts   = 5 * 4
    return {
                "mon" : make_list(weekday_shifts)
                "tue" : make_list(weekday_shifts)
                "wed" : make_list(weekday_shifts)
                "thu" : make_list(weekday_shifts)
                "fri" : make_list(weekday_shifts)
                "sun" : make_list(sunday_shifts)
            }

def make_list(length):
    shifts = []
    for shift in range(0, length-1):
        shifts.append([])
    return shifts

