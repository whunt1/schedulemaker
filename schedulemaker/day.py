from . import shift

class Day():

    def __init__(self):
        self.helpline_shifts = make_shifts(8, 21, True)
        self.clinic_shifts   = make_shifts(8, 17)

def make_shifts(start, end, helpline=False):
    shifts = []
    for shift_num in range(start*4, end*4):
        shifts.append(Shift(shift_num, helpline))
    return shifts
